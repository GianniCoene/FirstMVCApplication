﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyHobbies
{
    public enum Geslacht
    {
        Man,
        Vrouw
    }
    public class Band
    {

        public string Naam { get; set; }
        public int Jaar { get; set; }
        public List<BandLid> Bandleden { get; set; }
        public Band()
        {
            Bandleden = new List<BandLid>();
        }
    }

    public class BandLid
    {

        public string Naam { get; set; }
        public int Leeftijd { get; set; }
        public Geslacht Geslacht { get; set; }
        public BandLid()
        {

        }
    }
}
